package com.company.skills

import com.company.Fight
import com.company.Mob

class LayOnHand : Skill() {
    override fun useSkill(fight: Fight, caster: Mob, target: Mob) {
        target.currentHitPoints = target.maxHitPoints;
    }
}