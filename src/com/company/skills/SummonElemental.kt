package com.company.skills

import com.company.Fight
import com.company.Mob
import com.company.enemies.StarElemental

class SummonElemental : Skill() {
    override fun useSkill(fight: Fight, caster: Mob, target: Mob) {

        var elemental:StarElemental = StarElemental()

        fight.addParticipant(elemental)
    }
}