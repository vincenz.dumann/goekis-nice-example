package com.company.skills

import com.company.Fight
import com.company.Mob

abstract class Skill {

    abstract fun useSkill(fight: Fight, caster: Mob, target: Mob)
}