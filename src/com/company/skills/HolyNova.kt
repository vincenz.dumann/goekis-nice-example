package com.company.skills

import com.company.Fight
import com.company.Mob

class HolyNova : Skill() {
    override fun useSkill(fight: Fight, caster: Mob, target: Mob) {
        for (player in fight.getAllPlayers()) {
            player.heal(100)
        }

        for (enemy in fight.getAllEnemies()) {
            enemy.damage(50)
        }
    }
}