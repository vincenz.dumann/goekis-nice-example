package com.company.skills

import com.company.Fight
import com.company.Mob

class AutoAttack : Skill() {
    override fun useSkill(fight: Fight, caster: Mob, target: Mob) {
        target.damage(10)
    }
}