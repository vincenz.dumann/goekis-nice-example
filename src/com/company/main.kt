package com.company

import com.company.classes.Paladin
import com.company.classes.Priest
import com.company.enemies.Algalon
import com.company.skills.*
import java.sql.DriverManager.println

fun main() {
    println("Hello, world!")

    // lets go algalon

    var algalon: Algalon = Algalon();
    var radbod: Paladin = Paladin("Radbod",
            isPlayer = true,
            maxHitPoints = 50000,
            currentHitPoints = 50000,
            skills = listOf(
                AutoAttack(),
                LayOnHand(),
                ShieldBash()
    ) )


    var cailea: Priest = Priest("Cailea",
            isPlayer = true,
            maxHitPoints = 20000,
            currentHitPoints = 20000,
            skills = listOf(
                    AutoAttack(),
                    HolyNova(),
                    HealingWord()
            ) )

    var algalonFight: Fight = Fight();
    algalonFight.addParticipant(cailea)
    algalonFight.addParticipant(radbod)
    algalonFight.addParticipant(algalon)


    // while fihgt is ongoing

    // iterate over participants
    // particant.useGlobalCd()


    var IgnisFight: Fight = Fight();
    algalonFight.addParticipant(cailea)
    algalonFight.addParticipant(radbod)
    // Ignis


}