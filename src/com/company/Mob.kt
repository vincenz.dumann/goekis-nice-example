package com.company

import com.company.skills.Skill


// All Participants in a Fight
abstract class Mob(val name: String,
                   val maxHitPoints: Int,
                   val skills: List<Skill> = listOf(),
                   var currentHitPoints: Int = maxHitPoints,
                   var alive: Boolean = true,
                   var isPlayer: Boolean ) {

    abstract fun useGlobalCd(fight: Fight, skill: Skill)

    fun heal(hitPoints: Int) {
        if (this.currentHitPoints < this.maxHitPoints) {
            if (currentHitPoints + hitPoints > maxHitPoints) {
                this.currentHitPoints = this.maxHitPoints;
            } else {
                this.currentHitPoints = currentHitPoints + hitPoints;
            }
        }
    }

    fun damage(hitPoints: Int) {
        this.currentHitPoints = this.currentHitPoints - hitPoints;

        if (currentHitPoints < 0) {
            this.alive = false;
        }
    }
}