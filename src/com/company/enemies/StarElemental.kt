package com.company.enemies

import com.company.Fight
import com.company.Mob
import com.company.skills.AutoAttack
import com.company.skills.Skill

class StarElemental(name: String = "Star Elemental",
                    skills: List<Skill> = listOf(
                      AutoAttack()
              ),
                    hitPoints: Int = 1000,
                    isPlayer: Boolean = false) : Mob(name, skills, hitPoints) {

    override fun useGlobalCd(fight: Fight, skill: Skill) {
        TODO("Not yet implemented")
    }
}