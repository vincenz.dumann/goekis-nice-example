package com.company.enemies

import com.company.Fight
import com.company.Mob
import com.company.skills.AutoAttack
import com.company.skills.FallingStar
import com.company.skills.Skill
import kotlin.random.Random

class Algalon(name: String = "Algalon",
              skills: List<Skill> = listOf(
                      FallingStar(),
                      AutoAttack()
              ),
              isPlayer: Boolean = false,
              maxHitPoints: Int = 10000 ) : Mob() {

    override fun useGlobalCd(fight: Fight, skill: Skill) {
        val randomNumber = Random.nextInt(1, 101)

        if (randomNumber < 50) {
            // use auto Attack
        }

    }
}