package com.company

import kotlin.random.Random

class Fight {
    public val participants: MutableList<Mob> = mutableListOf()

    fun addParticipant(participant: Mob) {
        participants.add(participant)
    }

    fun getAllPlayers(): List<Mob> {
        return participants.filter { it.isPlayer and it.alive }
    }

    fun getAllEnemies(): List<Mob> {
        return participants.filter { !it.isPlayer and it.alive }
    }

    fun pickRandomPlayer(): Mob? {
        val players = getAllPlayers()
        return if (players.isNotEmpty()) {
            val randomIndex = Random.nextInt(players.size)
            players[randomIndex]
        } else {
            null
        }
    }

    fun fightStillOngoing() {
        // return false if all enemies are dead or all players are dead ...
    }

}
